var amqp = require("amqplib/callback_api");
const transportController = require("./api/controllers/transport");
const { transportState } = require("./api/models/transport");

const startConsumer = () => {
  const amqpURL = process.env.AMQP_URL;
  amqp.connect(amqpURL, function(error0, connection) {
    if (error0) {
      throw error0;
    }
    connection.createChannel(function(error1, channel) {
      if (error1) {
        throw error1;
      }
      const exchange = "restaurant." + process.env.NODE_ENV; //criado para produção e local

      channel.assertExchange(exchange, "direct", { //tipo de mensagem
        durable: false
      });

      channel.assertQueue(
        "transport.management." + process.env.NODE_ENV + ".queue",
        {
          exclusive: false
        },
        function(error2, q) { 
          console.log(" [*] Waiting for logs. To exit press CTRL+C");

          channel.bindQueue(q.queue, exchange, "order.cooked"); //dizer o que cada queue vai fazer
          channel.consume(
            q.queue,
            function(msg) {
              console.log(" [x] %s: '%s'",msg.fields.routingKey,msg.content.toString());
              proccessMessage(JSON.parse(msg.content.toString())); //logica do processo
            },
            {
              noAck: true
            }
          );
        }
      );
    });
  });
};

const proccessMessage = message => {
  console.log(message);
  switch (message.event) {
    case "order.cooked":
      console.log("Order Cooked. Transport");
      transportController.sendTransport(message.object);
      break;
    default:
      console.warn("Can't treat ", message.event);
    break;
  }
};

module.exports = startConsumer;
