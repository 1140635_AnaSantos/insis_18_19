require("dotenv").config();

const createChannel = async callback => {
    const channel = async () => {
        const conn = await require("amqplib").connect(process.env.AMQP_URL);

        return conn.createChannel().then(channel => {
            return Promise.resolve(channel);
        });
    };

    global.globalRabbitMQChannel = await channel();
    callback();
};

const sendMessage = async (routingKey, message) => {
    const exchange = "restaurant." + process.env.NODE_ENV;

    globalRabbitMQChannel.assertExchange(exchange, "direct", {
        durable: false
    });
    globalRabbitMQChannel.publish(exchange, routingKey, Buffer.from(message));
};

module.exports = { createChannel, sendMessage };
