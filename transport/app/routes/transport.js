const express = require("express");
const router = express.Router();
const transportCtrl = require("../../app/api/controllers/transport");

router.get("/", transportCtrl.getAll);
router.post("/", transportCtrl.create);
router.get("/:id", transportCtrl.getById);
router.put("/:id", transportCtrl.updateById);
router.delete("/:id", transportCtrl.deleteById);

module.exports = router;