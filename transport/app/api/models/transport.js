const mongoose = require('mongoose');
//Define a schema
const Schema = mongoose.Schema;

const TransportSchema = new Schema({
 name: {
  type: String,
  trim: true,  
  required: true,
 },
 quantity: {
  type: Number,
  trim: true,
  required: true
 }
});

module.exports = mongoose.model('Transport', TransportSchema);