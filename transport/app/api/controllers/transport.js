const transportModel = require("../models/transport");
const { sendMessage } = require("../../producer");

module.exports = {
  getById: function(req, res, next) {
    console.log("getting transport with id", req.params.id);
    transportModel.findById(req.params.id, function(err, transportInfo) {
      if (err) {
        next(err);
      } else {
        if (!!transportInfo) {
          res.json({
            status: "success",
            message: "Transport found!!!",
            data: { transport: transportInfo }
          });
        } else {
          console.log("Transport with id", req.params.id, "not found");
          res.status(404).json({ message: "Not found" });
        }
      }
    });
  },

  getAll: function(req, res, next) {
    console.log("getting all transports");
    let transportsList = [];

    transportModel.find({}, function(err, transports) {
      if (err) {
        next(err);
      } else {
        for (let transport of transports) {
            transportsList.push({
            id: transport._id,
            name: transport.name,
            quantity: transport.quantity
          });
        }
        res.json({
          status: "success",
          message: "Transport list found!!!",
          data: { transports: transportsList }
        });
      }
    });
  },

  updateById: function(req, res, next) {
    console.log("update transport with id", req.body.id);
    transportModel.findByIdAndUpdate(
      req.params.id,
      {
        name: req.body.name,
        quantity: req.body.quantity
      },
      function(err, transportInfo) {
        if (err) next(err);
        else {
          if (!!transportInfo) {
            res.json({
              status: "success",
              message: "Transport updated successfully!!!",
              data: null
            });
          } else {
            console.log("Transport with id", req.params.id, "not found");
            res.status(404).json({ message: "Not found" });
          }
        }
      }
    );
  },

  deleteById: function(req, res, next) {
    console.log("deleting transport with id", req.params.id);
    transportModel.findByIdAndRemove(req.params.id, function(err, transportInfo) {
      if (err) next(err);
      else {
        res.json({
          status: "success",
          message: "Transport deleted successfully!!!",
          data: null
        });
      }
    });
  },

  initiazeTransport: function(id) {
    console.log("initiliazing transport with id ", id);
    transportModel.findByIdAndUpdate(
      id,
      {
        state: transportState.INITIALIZED
      },
      function(err, transportInfo) {
        if (err) console.warn("Failed to initialize transport with id", id);
        else {
          if (!!transportInfo) {
            console.log("transport with id", id, "was initialized with success");
            //Transport Initialized
          } else {
            console.log("Transport with id", req.params.id, "not found");
            res.status(404).json({ message: "Not found" });
          }
        }
      }
    );
  },

  create: function(req, res, next) {
    console.log(
      "creating Transport with the params: ",
      req.body.name,
      req.body.quantity
    );
    transportModel.create(
      {
        name: req.body.name,
        quantity: req.body.quantity
      },
      function(err, result) {
        if (err) next(err);
        else
          // publishMessage(
          //   "transport.created",
          //   JSON.stringify({ event: "transport.created", object: result })
          // );
        res.json({
          status: "success",
          message: "Transport added successfully!!!",
          data: null
        });
      }
    );
  },

  sendTransport: function(order){
    console.log("start transport")
    setTimeout(function(){
      sendMessage(
        "order.completed",
        JSON.stringify({ event: "order.completed", object:  order })
      );
    }, 3000);
  }
};

