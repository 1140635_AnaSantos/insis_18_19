const express = require("express");
const router = express.Router();
const orderCtrl = require("../../app/api/controllers/order");

router.get("/", orderCtrl.getAll);
router.post("/", orderCtrl.create);
router.put("/:id", orderCtrl.updateById);
router.delete("/:id", orderCtrl.deleteById);

module.exports = router;
