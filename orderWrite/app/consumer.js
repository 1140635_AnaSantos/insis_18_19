var amqp = require("amqplib/callback_api");
const orderController = require("./api/controllers/order");

const startConsumer = () => {
  const amqpURL = process.env.AMQP_URL;
  amqp.connect(amqpURL, function(error0, connection) {
    if (error0) {
      throw error0;
    }
    connection.createChannel(function(error1, channel) {
      if (error1) {
        throw error1;
      }
      const exchange = "restaurant." + process.env.NODE_ENV; //criado para produção e local

      channel.assertExchange(exchange, "direct", { //tipo de mensagem
        durable: false
      });

      channel.assertQueue(
        "orderwrite.management." + process.env.NODE_ENV + ".queue",
        {
          exclusive: false
        },
        function(error2, q) { 
          console.log(" [*] Waiting for logs. To exit press CTRL+C");

          channel.bindQueue(q.queue, exchange, "order.error"); //dizer o que cada queue vai fazer
          channel.bindQueue(q.queue, exchange, "order.confirmed");
          channel.bindQueue(q.queue, exchange, "order.ready");
          channel.bindQueue(q.queue, exchange, "order.cooked");
          channel.bindQueue(q.queue, exchange, "order.completed");

          channel.consume(
            q.queue,
            function(msg) {
              console.log(" [x] %s: '%s'",msg.fields.routingKey,msg.content.toString());
              proccessMessage(JSON.parse(msg.content.toString())); //logica do processo
            },
            {
              noAck: true
            }
          );
        }
      );
    });
  });
};

const proccessMessage = message => {
  // console.log(message);
  // switch (message.event) {
  //   case 'order.error':
  //     console.log('Order Error');
  //     orderController.updateState(message.object._id, 1);
  //     break;
  //   case 'order.confirmed':
  //     console.log('Order Confirmed');
  //     orderController.updateState(message.object._id, 2);
  //     break;
  //   case 'order.ready':
  //     console.log('Order Ready');
  //     orderController.updateState(message.object._id, 3);
  //     break;
  //   case 'order.cooked':
  //     console.log('Order Cooked');
  //     orderController.updateState(message.object._id, 4);
  //     break;
  //   case 'order.completed':
  //     console.log('Order Completed');
  //     orderController.updateState(message.object._id, 5);
  //     break;
  //   default:
  //     console.warn("Can't treat ", message.event);
  //   break;
  // }
  console.log('Save Event', message)
  orderController.newOrderEvent(message.object, message.event);

};

module.exports = startConsumer;
