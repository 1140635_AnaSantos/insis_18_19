const { orderModel, orderState } = require("../models/order");
const { sendMessage } = require("../../producer");
const uuidv4 = require("uuid/v4");

module.exports = {
  getAll: function(req, res, next) {
    console.log("getting all orders");
    let ordersList = [];

    orderModel.find({}, function(err, orders) {
      if (err) {
        next(err);
      } else {
        for (let order of orders) {
          ordersList.push({
            order_id: order.order_id,
            event_name: order.event_name
          });
        }
        res.json({
          status: "success",
          message: "Order list found!!!",
          data: { orders: ordersList }
        });
      }
    });
  },

  updateById: function(req, res, next) {
    console.log("update order with id", req.body.id);
    orderModel.findByIdAndUpdate(
      req.params.id,
      {
        order_id: req.body.order_id,
        event_name: req.body.event_name
      },
      function(err, orderInfo) {
        if (err) next(err);
        else {
          if (!!orderInfo) {
            res.json({
              status: "success",
              message: "Order updated successfully!!!",
              data: null
            });
          } else {
            console.log("Order with id", req.params.id, "not found");
            res.status(404).json({ message: "Not found" });
          }
        }
      }
    );
  },

  deleteById: function(req, res, next) {
    console.log("deleting order with id", req.params.id);
    orderModel.findByIdAndRemove(req.params.id, function(err, orderInfo) {
      if (err) next(err);
      else {
        res.json({
          status: "success",
          message: "Order deleted successfully!!!",
          data: null
        });
      }
    });
  },

  create: function(req, res, next) {
    console.log(
      "creating order with the params: ",
      req.body.dish_id,
      req.body.released_on,
      req.body.client_id,
      req.body.service
    );
    var order_id = uuidv4();
    orderModel.create(
      {
        order_id: order_id,
        event_name: "order.create"
      },
      function(err, result) {
        if (err) next(err);
        else
          sendMessage(
            "order.create",
            JSON.stringify({ event: "order.create", object: {
              _id: order_id,
              dish_id: req.body.dish_id,
              released_on: req.body.released_on,
              client_id: req.body.client_id,
              service: req.body.service
            } })
          );
        res.json({
          status: "success",
          message: "Order added successfully!!!",
          data: null
        });
      }
    );
  },

  updateState: function(order_id, new_state) {
    console.log("update state with id", order_id);
    orderModel.findByIdAndUpdate(
      order_id,
      {
        state: new_state
      },
      function(err, orderInfo) {
        if (err) next(err);
        else {
          if (!!orderInfo) {
            console.log('Success, order updated successfully!');
          } else {
            console.log("Order with id", req.params.id, "not found");
           // res.status(404).json({ message: "Not found" });
          }
        }
      }
    );
  },

  newOrderEvent: function(order, event) {
    console.log("adding event to order", order);
    orderModel.create(
      {
        order_id: order._id,
        event_name: event
      },
      function(err, result) {
        if (err) console.log(err);
        else
          console.log( "event", event, "for order", order._id, "added successfully to the event store");
      }
    );
  }
};

