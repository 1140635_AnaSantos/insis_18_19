const mongoose = require("mongoose");
const uuidv4 = require("uuid/v4");

const OrderState = {
  CREATED: 0,
  CANCELED: 1,
  CONFIRMED: 2,
  READY: 3,
  COOKED: 4,
  COMPLETED: 5
};

const Schema = mongoose.Schema;

const OrderSchema = new Schema({
  order_id: {
    type: String,
    trim: true,
    required: true
  },
  event_name: {
    type: String,
    trim: true,
    required: true
  }
});

module.exports = {
  orderModel: mongoose.model("OrderEvent", OrderSchema),
  orderState: OrderState
};

