const stockModel = require("../models/stock");
const { sendMessage } = require("../../producer");

module.exports = {
  getById: function(req, res, next) {
    console.log("getting stock with id", req.params.id);
    stockModel.findById(req.params.id, function(err, stockInfo) {
      if (err) {
        next(err);
      } else {
        if (!!stockInfo) {
          res.json({
            status: "success",
            message: "Stock found!!!",
            data: { stock: stockInfo }
          });
        } else {
          console.log("Stock with id", req.params.id, "not found");
          res.status(404).json({ message: "Not found" });
        }
      }
    });
  },

  getAll: function(req, res, next) {
    console.log("getting all stocks");
    let stocksList = [];

    stockModel.find({}, function(err, stocks) {
      if (err) {
        next(err);
      } else {
        for (let stock of stocks) {
            stocksList.push({
            id: stock._id,
            ingredient: stock.ingredient,
            quantity: stock.quantity,
            ingredientCode: stock.ingredientCode
          });
        }
        res.json({
          status: "success",
          message: "Stock list found!!!",
          data: { stocks: stocksList }
        });
      }
    });
  },

  updateById: function(req, res, next) {
    console.log("update stock with id", req.body.id);
    stockModel.findByIdAndUpdate(
      req.params.id,
      {
        ingredient: req.body.ingredient,
        quantity: req.body.quantity,
        ingredientCode: req.body.ingredientCode
      },
      function(err, stockInfo) {
        if (err) next(err);
        else {
          if (!!stockInfo) {
            res.json({
              status: "success",
              message: "Stock updated successfully!!!",
              data: null
            });
          } else {
            console.log("Stock with id", req.params.id, "not found");
            res.status(404).json({ message: "Not found" });
          }
        }
      }
    );
  },

  deleteById: function(req, res, next) {
    console.log("deleting stock with id", req.params.id);
    stockModel.findByIdAndRemove(req.params.id, function(err, stockInfo) {
      if (err) next(err);
      else {
        res.json({
          status: "success",
          message: "Stock deleted successfully!!!",
          data: null
        });
      }
    });
  },

  initiazeStock: function(id) {
    console.log("initiliazing stock with id ", id);
    stockModel.findByIdAndUpdate(
      id,
      {
        state: stockState.INITIALIZED
      },
      function(err, stockInfo) {
        if (err) console.warn("Failed to initialize stock with id", id);
        else {
          if (!!stockInfo) {
            console.log("stock with id", id, "was initialized with success");
            //Stock Initialized
          } else {
            console.log("Stock with id", req.params.id, "not found");
            res.status(404).json({ message: "Not found" });
          }
        }
      }
    );
  },

  create: function(req, res, next) {
    console.log(
      "creating Stock with the params: ",
      req.body.ingredient,
      req.body.quantity,
      req.body.ingredientCode
    );
    stockModel.create(
      {
        ingredient: req.body.ingredient,
        quantity: req.body.quantity,
        ingredientCode: req.body.ingredientCode
      },
      function(err, result) {
        if (err) next(err);
        else
          // publishMessage(
          //   "stock.created",
          //   JSON.stringify({ event: "stock.created", object: result })
          // );
        res.json({
          status: "success",
          message: "Stock added successfully!!!",
          data: null
        });
      }
    );
  },

  checkIngredients: function(order, dish) {
    stockModel.find({}, function(err, results) {
      let canCook = true;
      console.log("ingredients", dish.ingredients);
      dish.ingredients.filter(ingredient => {
        const result = results.find(
          element => element.ingredientCode === ingredient.code
        );
        if (!result || (!!result && result.quantity < ingredient.quantity)) {
          canCook = false;
          return;
        }
      });
      if (canCook) {
        dish.ingredients.filter(ingredient => {
          const result = results.find(
            element => element.ingredientCode === ingredient.code
          );
          if (
            !(!result || (!!result && result.quantity < ingredient.quantity))
          ) {
            stockModel.findByIdAndUpdate(
              result._id,
              {
                ingredientCode: result.ingredientCode,
                quantity: Number(result.quantity) - Number(ingredient.quantity)
              },
              function(err, stockInfo) {
                if (err) {
                  console.log("could not update ingredients");
                } else {
                  if (!!stockInfo) {
                    console.log("Stock updated successfully!!!");
                    sendMessage("order.ready", JSON.stringify({ event: "order.ready", object: order }));
                  } else {
                    console.log("Stock with id", result._id, "not found");
                  }
                }
              }
            );
          }
        });
      } else {
        console.log("no ingredients");
        sendMessage("order.error", JSON.stringify({ event: "order.error", object:  order }));
      }
    });
  }
};

