const mongoose = require('mongoose');
//Define a schema
const Schema = mongoose.Schema;

const StockSchema = new Schema({
 ingredient: {
  type: String,
  trim: true,  
  required: true,
 },
 quantity: {
  type: Number,
  trim: true,
  required: true
 },
 ingredientCode: {
  type: String,
  trim: true,  
  required: true,
 }
});

module.exports = mongoose.model('Stock', StockSchema);