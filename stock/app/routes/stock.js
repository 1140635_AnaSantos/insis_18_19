const express = require("express");
const router = express.Router();
const stockCtrl = require("../api/controllers/stock");

router.get("/", stockCtrl.getAll);
router.post("/", stockCtrl.create);
router.get("/:id", stockCtrl.getById);
router.put("/:id", stockCtrl.updateById);
router.delete("/:id", stockCtrl.deleteById);

module.exports = router;