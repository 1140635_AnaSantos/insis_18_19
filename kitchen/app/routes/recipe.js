const express = require("express");
const router = express.Router();
const recipeCtrl = require("../api/controllers/recipe");

router.get("/", recipeCtrl.getAll);
router.post("/", recipeCtrl.create);
router.get("/:id", recipeCtrl.getById);
router.put("/:id", recipeCtrl.updateById);
router.delete("/:id", recipeCtrl.deleteById);

module.exports = router;
