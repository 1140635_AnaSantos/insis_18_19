const mongoose = require("mongoose");
const uuidv4 = require("uuid/v4");

const Schema = mongoose.Schema;

const RecipeSchema = new Schema({
  name: {
    type: String,
    trim: true,
    required: true
  },
  price: {
    type: String,
    trim: true,
    required: true,
  },
  ingredients: [{
    code: {
      type: String,
      trim: true,
      required: true
    },
    quantity: {
      type: Number,
      required: true
    }
  }]
});

module.exports = mongoose.model('Recipe', RecipeSchema);
