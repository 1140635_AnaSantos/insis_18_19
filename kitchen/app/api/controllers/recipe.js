const recipeModel = require("../models/recipe");
const {sendMessage} = require("../../producer");

module.exports = {
  getById: function (req, res, next) {
    console.log("getting recipe with id", req.params.id);
    recipeModel.findById(req.params.id, function (err, recipeInfo) {
      if (err) {
        next(err);
      } else {
        if (!!recipeInfo) {
          res.json({
            status: "success",
            message: "Recipe found!!!",
            data: { recipe: recipeInfo }
          });
        } else {
          console.log("Recipe with id", req.params.id, "not found");
          res.status(404).json({ message: "Not found" });
        }
      }
    });
  },

  getAll: function (req, res, next) {
    console.log("getting all recipes");
    let recipesList = [];

    recipeModel.find({}, function (err, recipes) {
      if (err) {
        next(err);
      } else {
        for (let recipe of recipes) {
          recipesList.push({
            id: recipe._id,
            name: recipe.name,
            price: recipe.price,
            ingredients: recipe.ingredients
          });
        }
        res.json({
          status: "success",
          message: "Recipes list found!!!",
          data: { recipes: recipesList }
        });
      }
    });
  },

  updateById: function (req, res, next) {
    console.log("update recipe with id", req.body.id);
    recipeModel.findByIdAndUpdate(
      req.params.id,
      {
        name: req.body.name,
        price: req.body.price,
        ingredients: req.body.ingredients,
      },
      function (err, recipeInfo) {
        if (err) next(err);
        else {
          if (!!recipeInfo) {
            res.json({
              status: "success",
              message: "Recipe updated successfully!!!",
              data: null
            });
          } else {
            console.log("Recipe with id", req.params.id, "not found");
            res.status(404).json({ message: "Not found" });
          }
        }
      }
    );
  },

  deleteById: function (req, res, next) {
    console.log("deleting recipe with id", req.params.id);
    recipeModel.findByIdAndRemove(req.params.id, function (err, recipeInfo) {
      if (err) next(err);
      else {
        res.json({
          status: "success",
          message: "Recipe deleted successfully!!!",
          data: null
        });
      }
    });
  },

  create: function (req, res, next) {
    console.log(
      "creating recipe with the params: ",
      req.body.name,
      req.body.price,
      req.body.ingredients
    );
    recipeModel.create(
      {
        name: req.body.name,
        price: req.body.price,
        ingredients: req.body.ingredients
      },
      function (err, result) {
        if (err) next(err);
        else
          res.json({
            status: "success",
            message: "Recipe added successfully!!!",
            data: null
          });
      }
    );
  },

  startKitchen: function (order) {
    recipeModel.findById(order.dish_id, function (err, dishInfo) {
      if (err) {
        console.log("cannot initialize order with id:", order._id)
      } else {
        console.log("Cuisine initializing order with id:",order._id,"and with dish_id:",order.dish_id);
        console.log(dishInfo);
        sendMessage(
          "stock.checkIngredients",
          JSON.stringify({ event: "stock.checkIngredients", object: { order, dish: dishInfo } })
        );
      }
    });
  },

  startCook: function (order) {
    console.log("start cooking")
    setTimeout(function(){
      sendMessage(
        "order.cooked",
        JSON.stringify({ event: "order.cooked", object:  order })
      );
    }, 3000);   
  }
};

