const express = require('express'); 
const logger = require('morgan');
const bodyParser = require('body-parser');
const app = express();
const recipeRoutes = require('./app/routes/recipe');
const mongoose = require("./app/config/database");
const { createChannel, sendMessage } = require("./app/producer");
const startConsumer = require("./app/consumer");

const start = () => {
    // Database
    mongoose.connection.on(
        "error",
        console.error.bind(console, "MongoDB connection error:")
    );

    app.use(logger('dev'));
    const port = process.env.PORT || 3000
    app.use(bodyParser.json());
    app.use("/recipe", recipeRoutes);

    app.get('/', function(req, res){
    res.json({"tutorial" : "Build REST API with node.js"});
    });
    app.listen(port, function(){ console.log('Node server listening on port',port);});

    startConsumer();
}

createChannel(start);
