const mongoose = require("mongoose");
const uuidv4 = require("uuid/v4");

const OrderState = {
  CREATED: 0,
  CANCELED: 1,
  CONFIRMED: 2,
  READY: 3,
  COOKED: 4,
  COMPLETED: 5
};

const Schema = mongoose.Schema;

const OrderSchema = new Schema({
  _id: {
    type: String,
    trim: true,
    required: true
  },
  dish_id: {
    type: String,
    trim: true,
    required: true
  },
  released_on: {
    type: Date,
    trim: true,
    required: true
  },
  client_id: {
    type: String,
    trim: true,
    required: true
  },
  service: {
    type: String,
    trim: true,
    required: true
  },
  state: {
    type: Number,
    default: OrderState.CREATED,
    required: false
  }
});

module.exports = {
  orderModel: mongoose.model("Order", OrderSchema),
  orderState: OrderState
};

