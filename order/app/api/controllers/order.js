const { orderModel, orderState } = require("../models/order");
const { sendMessage } = require("../../producer");

module.exports = {
  getById: function(req, res, next) {
    console.log("getting oorder with id", req.params.id);
    orderModel.findById(req.params.id, function(err, orderInfo) {
      if (err) {
        next(err);
      } else {
        if (!!orderInfo) {
          res.json({
            status: "success",
            message: "Order found!!!",
            data: { order: orderInfo }
          });
        } else {
          console.log("Order with id", req.params.id, "not found");
          res.status(404).json({ message: "Not found" });
        }
      }
    });
  },

  getAll: function(req, res, next) {
    console.log("getting all orders");
    let ordersList = [];

    orderModel.find({}, function(err, orders) {
      if (err) {
        next(err);
      } else {
        for (let order of orders) {
          ordersList.push({
            id: order._id,
            dish_id: order.dish_id,
            released_on: order.released_on,
            client_id: order.client_id,
            service: order.service,
            state: order.state
          });
        }
        res.json({
          status: "success",
          message: "Order list found!!!",
          data: { orders: ordersList }
        });
      }
    });
  },

  updateById: function(req, res, next) {
    console.log("update order with id", req.body.id);
    orderModel.findByIdAndUpdate(
      req.params.id,
      {
        dish_id: req.body.dish_id,
        released_on: req.body.released_on,
        client_id: req.body.client_id,
        service: req.body.service,
        state: req.body.state
      },
      function(err, orderInfo) {
        if (err) next(err);
        else {
          if (!!orderInfo) {
            res.json({
              status: "success",
              message: "Order updated successfully!!!",
              data: null
            });
          } else {
            console.log("Order with id", req.params.id, "not found");
            res.status(404).json({ message: "Not found" });
          }
        }
      }
    );
  },

  deleteById: function(req, res, next) {
    console.log("deleting order with id", req.params.id);
    orderModel.findByIdAndRemove(req.params.id, function(err, orderInfo) {
      if (err) next(err);
      else {
        res.json({
          status: "success",
          message: "Order deleted successfully!!!",
          data: null
        });
      }
    });
  },

  initiazeOrder: function(id) {
    console.log("initiliazing order with id ", id);
    orderModel.findByIdAndUpdate(
      id,
      {
        state: orderState.INITIALIZED
      },
      function(err, orderInfo) {
        if (err) console.warn("Failed to initialize order with id", id);
        else {
          if (!!orderInfo) {
            console.log("order with id", id, "was initialized with success");
            //Order Initialized
          } else {
            console.log("Order with id", req.params.id, "not found");
            res.status(404).json({ message: "Not found" });
          }
        }
      }
    );
  },

  // create: function(req, res, next) {
  //   console.log(
  //     "creating order with the params: ",
  //     req.body.dish_id,
  //     req.body.released_on,
  //     req.body.client_id,
  //     req.body.service
  //   );
  //   orderModel.create(
  //     {
  //       dish_id: req.body.dish_id,
  //       released_on: req.body.released_on, 
  //       client_id: req.body.client_id,
  //       service: req.body.service
  //     },
  //     function(err, result) {
  //       if (err) next(err);
  //       else
  //         sendMessage(
  //           "order.created",
  //           JSON.stringify({ event: "order.created", object: result })
  //         );
  //       res.json({
  //         status: "success",
  //         message: "Order added successfully!!!",
  //         data: null
  //       });
  //     }
  //   );
  // },

  create: function(order) {
    console.log("creating order ",order);

    orderModel.create(
      {
        _id: order._id,
        dish_id: order.dish_id,
        released_on: order.released_on,
        client_id: order.client_id,
        service: order.service
      },
      function(err, result) {
        if (err) {
          console.log("could not add order");
        } else {
          sendMessage(
            "order.created",
            JSON.stringify({ event: "order.created", object: result })
          );
          console.log("order was added successfully");
        }
      }
    );
  },

  updateState: function(order_id, new_state) {
    console.log("update state with id", order_id);
    orderModel.findByIdAndUpdate(
      order_id,
      {
        state: new_state
      },
      function(err, orderInfo) {
        if (err) next(err);
        else {
          if (!!orderInfo) {
            console.log('Success, order updated successfully!');
          } else {
            console.log("Order with id", req.params.id, "not found");
           // res.status(404).json({ message: "Not found" });
          }
        }
      }
    );
  }
};

