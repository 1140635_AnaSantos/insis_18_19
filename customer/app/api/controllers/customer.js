const customerModel = require("../models/customer");
const { sendMessage } = require("../../producer");
const fs   = require('fs');

module.exports = {
  getById: function(req, res, next) {
    console.log("getting customer with id", req.params.id);
    customerModel.findById(req.params.id, function(err, customerInfo) {
      if (err) {
        next(err);
      } else {
        if (!!customerInfo) {
          res.json({
            status: "success",
            message: "Customer found!!!",
            data: { customer: customerInfo }
          });
        } else {
          console.log("Customer with id", req.params.id, "not found");
          res.status(404).json({ message: "Not found" });
        }
      }
    });
  },

  getAll: function(req, res, next) {
    console.log("getting all customers");
    let customersList = [];

    customerModel.find({}, function(err, customers) {
      if (err) {
        next(err);
      } else {
        for (let customer of customers) {
            customersList.push({
            id: customer._id,
            name: customer.name,
            cardNumber: customer.cardNumber
          });
        }
        res.json({
          status: "success",
          message: "Customer list found!!!",
          data: { customers: customersList }
        });
      }
    });
  },

  updateById: function(req, res, next) {
    console.log("update customer with id", req.body.id);
    customerModel.findByIdAndUpdate(
      req.params.id,
      {
        name: req.body.name,
        cardNumber: req.body.cardNumber
      },
      function(err, customerInfo) {
        if (err) next(err);
        else {
          if (!!customerInfo) {
            res.json({
              status: "success",
              message: "Customer updated successfully!!!",
              data: null
            });
          } else {
            console.log("Customer with id", req.params.id, "not found");
            res.status(404).json({ message: "Not found" });
          }
        }
      }
    );
  },

  deleteById: function(req, res, next) {
    console.log("deleting customer with id", req.params.id);
    customerModel.findByIdAndRemove(req.params.id, function(err, customerInfo) {
      if (err) next(err);
      else {
        res.json({
          status: "success",
          message: "Customer deleted successfully!!!",
          data: null
        });
      }
    });
  },

  initiazeCustomer: function(order,id) {
    console.log("initiliazing customer with id ", id);
    customerModel.findById(
      id,
      function(err, customerInfo) {
        if (err) 
        {
          sendMessage('order.error', JSON.stringify({ event: "order.error", object: order }));
          console.warn("Failed to initialize customer with id", id);
        }
        else {
          if (!!customerInfo) {
            sendMessage('order.confirmed', JSON.stringify({ event: "order.confirmed", object: order }));
            console.log("customer with id", id, "was initialized with success");
            //customer Initialized
          } else {
            console.log("customer with id", id, "not found");
            //res.status(404).json({ message: "Not found" });
          }
        }
      }
    );
  },

  create: function(req, res, next) {
    console.log(
      "creating customer with the params: ",
      req.body.name,
      req.body.cardNumber
    );
    customerModel.create(
      {
        name: req.body.name,
        cardNumber: req.body.cardNumber
      },
      function(err, result) {
        if (err) next(err);
        else
          // publishMessage(
          //   "customer.created",
          //   JSON.stringify({ event: "customer.created", object: result })
          // );
        res.json({
          status: "success",
          message: "customer added successfully!!!",
          data: null
        });
      }
    );
  },

  getSwaggerDocumentation: function (req, res) {
    var doc = fs.readFileSync('app/api/controllers/documentation.json', 'utf8');
    res.send(doc);
  }
};

