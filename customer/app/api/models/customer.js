const mongoose = require('mongoose');
//Define a schema
const Schema = mongoose.Schema;

const CustomerSchema = new Schema({
 name: {
  type: String,
  trim: true,  
  required: true,
 },
 cardNumber: {
  type: Number,
  trim: true,
  required: true
 }
});

module.exports = mongoose.model('Customer', CustomerSchema);