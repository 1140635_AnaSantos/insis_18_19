const express = require("express");
const router = express.Router();
const customerCtrl = require("../../app/api/controllers/customer");

router.get("/documentation", customerCtrl.getSwaggerDocumentation);
router.get("/", customerCtrl.getAll);
router.post("/", customerCtrl.create);
router.get("/:id", customerCtrl.getById);
router.put("/:id", customerCtrl.updateById);
router.delete("/:id", customerCtrl.deleteById);

module.exports = router;
