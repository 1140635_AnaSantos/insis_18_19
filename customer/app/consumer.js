var amqp = require("amqplib/callback_api");
const customerController = require("./api/controllers/customer");
const { customerState } = require("./api/models/customer");

const startConsumer = () => {
  const amqpURL = process.env.AMQP_URL;
  amqp.connect(amqpURL, function(error0, connection) {
    if (error0) {
      throw error0;
    }
    connection.createChannel(function(error1, channel) {
      if (error1) {
        throw error1;
      }
      const exchange = "restaurant." + process.env.NODE_ENV; //criado para produção e local

      channel.assertExchange(exchange, "direct", { //tipo de mensagem
        durable: false
      });

      channel.assertQueue(
        "customer.management." + process.env.NODE_ENV + ".queue",
        {
          exclusive: false
        },
        function(error2, q) { 
          console.log(" [*] Waiting for logs. To exit press CTRL+C");

          //channel.bindQueue(q.queue, exchange, "customer.initialized"); //dizer o que cada queue vai fazer
          channel.bindQueue(q.queue, exchange, "order.created");
          channel.bindQueue(q.queue, exchange, "order.completed");

          channel.consume(
            q.queue,
            function(msg) {
              console.log(" [x] %s: '%s'",msg.fields.routingKey,msg.content.toString());
              proccessMessage(JSON.parse(msg.content.toString())); //logica do processo
            },
            {
              noAck: true
            }
          );
        }
      );
    });
  });
};

const proccessMessage = message => {
  console.log(message);
  switch (message.event) {
    case 'order.created':
      customerController.initiazeCustomer(message.object, message.object.client_id);
      break;
    case 'order.completed':
      console.log("COMPLETED");
      break;
    default:
      console.warn("Can't treat ", message.event);
    break;
  }
};

module.exports = startConsumer;
