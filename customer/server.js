const express = require('express'); 
const logger = require('morgan');
const bodyParser = require('body-parser');
const app = express();
const customerRoutes = require('./app/routes/customer');
const mongoose = require("./app/config/database");
const { createChannel, sendMessage } = require("./app/producer");
const startConsumer = require("./app/consumer");

const start = () => {
// Database
 mongoose.connection.on(
     "error",
     console.error.bind(console, "MongoDB connection error:")
 );
 const port = process.env.PORT || 3000
 app.use(logger('dev'));
 app.use(bodyParser.urlencoded({extended: false}));
 app.use("/customer", customerRoutes);

 app.get('/', function(req, res){
  res.json({"tutorial" : "Build REST API with node.js"});
 });
 app.listen(port, function(){ console.log('Node server listening on port',port);});

 startConsumer();

};

createChannel(start);
